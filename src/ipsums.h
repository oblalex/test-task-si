#ifndef TEST_TASK_IPSUMS
#define TEST_TASK_IPSUMS

const char* const ipsums[] =
{
    "Here men from the planet Earth first set foot upon the Moon. "
        "July 1969 AD. We came in peace for all mankind.",
    "Brother Anancy had just bought a sporty new car and offered to drive.",
    "Consequat baboiii gelatooo tulaliloo pepete.",
    "Problems look mighty small from 150 miles up.",
    "Aged id extra blue mountain beans body doppio coffee seasonal.",
    "Ah coodle doodle doo, ah coodle doodle doo. Who?",
    "Dynamically innovate resource-leveling customer service for state of the "
        "art customer service.",
    "Takin' a break from all your worries sure would help a lot.",
    "Zombie ipsum reversus ab viral inferno, nam rick grimes malum cerebro.",
    "The lakes naff chippy conked him one on the nose, doofer.",
    "Bavarian bergkase rubber cheese cheesy grin feta cheese on toast cheese "
        "strings blue castello bavarian bergkase.",
    "You think water moves fast? You should see ice. It moves like it has a "
        "mind.",
    "Fallen eyebrow lando calrissian terry thomas middle eastern despot derek "
        "griffiths mexican gunslinger wario?",
    "Emerged into consciousness! Something incredible is waiting to be known "
        "colonies the only home we've ever known network of wormholes "
        "tendrils of gossamer clouds another world Orion's sword worldlets.",
    "Science has not yet mastered prophecy. We predict too much for the next "
        "year and yet far too little for the next 10.",
    "Pork loin short ribs pig, shoulder chuck fatback rump tenderloin "
        "pastrami leberkas chicken pork belly ham tongue.",
    "The dreams of yesterday are the hopes of today and the reality of "
        "tomorrow.",
    "Ornare id lectus cras pharetra faucibus tristique nullam non accumsan "
        "justo nulla.",
    "Bowsprit line Arr Sea Legs jack lad reef transom boatswain hang the jib.",
    "Spaghetti bowl cosmopolitan chateau the mgm grand bankroll; the "
        "chandelier tease stratosphere payoff?",
    "Antarctic icefish wallago jewel tetra electric ray cusk-eel blackfish "
        "slender mola sweeper orangespine unicorn fish.",
    "I don't know what you could say about a day in which you have seen four "
        "beautiful sunsets."
};

#endif // TEST_TASK_IPSUMS
