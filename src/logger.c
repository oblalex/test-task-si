#include <errno.h>
#include <libgen.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>

#include "commons.h"


#define MAX_CONNECTIONS 2
#define ARG_KEY_HELP "-h"

#define LOG(msg) enlog(ctx.logger, msg)


void init(int argc, char** argv);
void validate_args(int argc, char** argv);
void show_usage_n_halt(char* prog_name);
void show_usage(char* prog_name);
void init_signals(void);
void on_int(int sig_num);
FILE* get_log_file(char* log_path);
int get_socket(void);
void enlog(FILE* logger, char* message);
void tear_down(void);


/* Storing global data in one structure */
struct
{
    FILE* logger;
    int server_sock;
} ctx;


int main(int argc, char** argv)
{
    init(argc, argv);

    int i, fd, client, ready_reads, rc, done, offset_in, offset_msg;
    int clients_num = 0;

    socklen_t client_addr_len;
    struct sockaddr_in client_addr;

    int buff_len = MSG_MAX_LENGTH + 1;
    char buff[buff_len];

    fd_set master_set, working_set;

    FD_ZERO(&master_set);
    FD_SET(ctx.server_sock, &master_set);

    while(1)
    {
        /* Update fd_set for reading */
        memcpy(&working_set, &master_set, sizeof(master_set));
        /*
         * Wait for read event from any socket. Use FD_SETSIZE as maximal file
         * descriptor number for simplicity
         */
        rc = select(FD_SETSIZE, &working_set, NULL, NULL, NULL);
        if (rc == -1)
        {
            perror("Select error");
            break;
        }
        ready_reads = 0;
        /*
         * Look through all possible file desctiptor numbers until all
         * available descriptors are processed
         */
        for (fd = 0; fd < FD_SETSIZE && ready_reads < rc; fd++)
        {
            /* Go next, if current descriptor is not ready */
            if (!FD_ISSET(fd, &working_set))
                continue;
            ready_reads++;
            /*
             * If current descriptor is our server socket, try to accept new
             * connection.
             */
            if (fd == ctx.server_sock)
            {
                client_addr_len = sizeof(client_addr);
                client = accept(ctx.server_sock,
                                (struct sockaddr *) &client_addr,
                                &client_addr_len);
                if (client == -1)
                {
                    perror("Client acception error");
                    continue;
                }
                /*
                 * Reject connection if maximal connections count is achieved
                 */
                if (clients_num >= MAX_CONNECTIONS)
                {
                    close(client);
                } else {
                /* Accept connection and mark it as a source of reading */
                    FD_SET(client, &master_set);
                    clients_num++;
                }
            } else {
                /*
                 * Current descriptor is client's socker. Clear messages buffer
                 */
                memset(buff, 0, buff_len);
                offset_in = 0;
                /* Try to read all available data from the socket */
                while ((done = recv(fd,
                                    buff + offset_in,
                                    buff_len - offset_in - 1,
                                    0)) > 0)
                {
                    offset_in += done;
                    /* Assume that the message is read if it ends with zero */
                    if (buff[offset_in-1] == '\0')
                        break;
                }
                /* Handle reading error */
                if (done < 0)
                {
                    if (errno == EAGAIN)
                        ;  /* No data to read. It is OK */
                    else
                        perror("Reading error");
                } else if (offset_in == 0) {
                    /* Client has disconnected */
                    clients_num--;
                    close(fd);
                    FD_CLR(fd, &master_set);
                } else {
                    /*
                     * Try to split zero-separated messages if there are some.
                     * This may happen if client has sent multiple messages
                     * since last read and they accumulated in single buffer
                     */
                    offset_msg = 0;
                    for (i = 0; i < buff_len; i++)
                    {
                        if (buff[i] != '\0')
                            continue;
                        if (i - offset_msg == 0)
                            break;
                        LOG(buff + offset_msg);
                        offset_msg = i + 1;
                    }
                }
            }
        }
    }

    tear_down();
    return EXIT_SUCCESS;
}


void init(int argc, char** argv)
{
    validate_args(argc, argv);
    init_signals();
    ctx.logger = get_log_file(argv[1]);
    ctx.server_sock = get_socket();
}


void validate_args(int argc, char** argv)
{
    char* prog_name = basename(argv[0]);
    /* User must enter exactly one argument */
    if (argc != 2)
        show_usage_n_halt(prog_name);
    /* Check if argument seems to not be a key */
    if (argv[1][0] != '-')
        return;
    /* Compare entered argument with known keys */
    if (strcmp(argv[1], ARG_KEY_HELP) == 0)
    {
        show_usage(prog_name);
        exit(EXIT_SUCCESS);
    }
    /* Unknown key was entered */
    show_usage_n_halt(prog_name);
}


void show_usage_n_halt(char* prog_name)
{
    fprintf(stderr, "ERROR: Invalid program usage! Read the help below.\n\n");
    show_usage(prog_name);
    exit(EXIT_FAILURE);
}


void show_usage(char* prog_name)
{
    printf("NAME\n\n\t");
    printf("%s - write messages to a file from other processes connected via "
           "TCP sockets", prog_name);

    printf("\n\nSYNOPSIS\n\n\t");
    printf("%s -h|LOG_PATH", prog_name);

    printf("\n\nDESCRIPTION\n\n\t");
    printf("Starts socket logger at %s:%d.\n\t", HOST_ADDR, LISTENING_PORT);
    printf("Writes all incoming zero-terninated messages to a file specified "
           "by LOG_PATH argument.\n\t");
    printf("There can be only %d clients connected at the same time.",
            MAX_CONNECTIONS);

    printf("\n\n\tLOG_PATH\t");
    printf("path to the file to write messages to");
    printf("\n\t-h\t\t");
    printf("show this help");

    printf("\n\nUSAGE EXAMPLE\n\n\t");
    printf("%s /tmp/ipc_test.log", prog_name);
    printf("\n\n");
}


/* Set up interruption signals handling to exit program in a proper way */
void init_signals(void)
{
    if (signal(SIGINT, on_int) == SIG_ERR)
    {
        perror("Failed to set handler for SIGPIPE");
        exit(EXIT_FAILURE);
    }
    if (signal(SIGTERM, on_int) == SIG_ERR)
    {
        perror("Failed to set handler for SIGTERM");
        exit(EXIT_FAILURE);
    }
}


/* Interruption handler */
void on_int(int sig_num)
{
    tear_down();
    exit(EXIT_SUCCESS);
}


/* Try to get file descriptor to write the log messages to */
FILE* get_log_file(char* log_path)
{
    FILE* fp;
    fp = fopen(log_path, "a");
    if (fp == NULL)
    {
        perror("Log openning error");
        exit(EXIT_FAILURE);
    }
    return fp;
}


/* Try to create a socket for listening clients */
int get_socket(void)
{
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1)
    {
        perror("Socket creation error");
        exit(EXIT_FAILURE);
    }

    socklen_t socksize = sizeof(struct sockaddr_in);
    struct sockaddr_in addr_info;

    memset(&addr_info, 0, socksize);
    addr_info.sin_family = AF_INET;
    addr_info.sin_addr.s_addr = inet_addr(HOST_ADDR);
    addr_info.sin_port = htons(LISTENING_PORT);

    if (bind(sock, (struct sockaddr *) &addr_info, socksize) == -1)
    {
        perror("Socket binding error");
        exit(EXIT_FAILURE);
    }
    if (listen(sock, MAX_CONNECTIONS) == -1)
    {
        perror("Socket listening error");
        exit(EXIT_FAILURE);
    }
    int on = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char *)&on,
                   sizeof(on)) == -1)
    {
        perror("setsockopt() error");
        close(sock);
        exit(EXIT_FAILURE);
    }
    return sock;
}


/*
 * Write a message to the specified log file in
 * '[YYYY-mm-dd HH:MM:SS] message' format
 */
void enlog(FILE* logger, char* message)
{
    time_t current_time;
    struct tm * time_info;
    char time_str[20];  // place for "YYYY-mm-dd HH:MM:SS\0"

    time(&current_time);
    time_info = localtime(&current_time);

    strftime(time_str, sizeof(time_str), "%F %T", time_info);
    fprintf(logger, "[%s] %s\n", time_str, message);
    fflush(logger);
}


void tear_down(void)
{
    close(ctx.server_sock);
    fclose(ctx.logger);
}
