#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "commons.h"
#include "ipsums.h"


#define MIN_MSG_INTERVAL 1
#define MAX_MSG_INTERVAL 5

#define PIPE_PREFIX "/tmp/test_worker"
#define ARG_KEY_HELP "-h"

#define LOG(msg, length) enlog(ctx.logger, msg, length)


void init(int argc, char** argv);
void validate_args(int argc, char** argv);
void show_usage_n_halt(char* prog_name);
void show_usage(char* prog_name);
void init_signals(void);
void on_int(int sig_num);
void on_alarm(int sig_num);
int get_worker_number(int argc, char** argv);
void get_pipes(char* self_path, int path_len, int self_num, int (*out)[2]);
int get_logger(void);
void enlog(int logger, char* msg, int length);
void tear_down(void);


/* Storing global data in one structure */
struct
{
    int self_num;
    int peer_num;
    int logger;
    int pipes[2];
    int msg_id;
    char self_path[sizeof(PIPE_PREFIX) + 3];
} ctx;


int main(int argc, char** argv)
{
    init(argc, argv);
    get_pipes(ctx.self_path, sizeof(ctx.self_path), ctx.self_num, &(ctx.pipes));

    /*
     * Init sending of messages. Do not start sending messages just after the
     * program has started
     */
    on_alarm(-1);

    int done, offset;
    int buff_len = MSG_MAX_LENGTH-10;
    char buff[buff_len];

    int msg_len;
    int msg_max_len = MSG_MAX_LENGTH+1;
    char msg[msg_max_len];

    while (1)
    {
        done = offset = 0;
        /* Read input pipe line-by-line in blocking mode */
        while((done = read(ctx.pipes[0], buff + offset, buff_len - offset - 1)) > 0)
        {
            offset += done;
            /* Whole line was received. Truncate it for further processing */
            if (buff[offset-1] == '\n')
            {
                buff[offset-1] = '\0';
                break;
            }
        }
        /* Nothing was read - connection with peer was lost */
        if (offset == 0)
            break;

        /* Enlog received message */
        memset(msg, 0, msg_max_len);
        msg_len = sprintf(msg, "#%d <- #%d: %s", ctx.self_num, ctx.peer_num, buff);
        LOG(msg, msg_len+1);
    }

    tear_down();
    return EXIT_SUCCESS;
}


void init(int argc, char** argv)
{
    validate_args(argc, argv);
    init_signals();
    srand(time(NULL));

    ctx.self_num = get_worker_number(argc, argv);
    ctx.logger = get_logger();

    /* Enlog process spawning */
    char msg[23];
    sprintf(msg, "Worker #%d has spawned.", ctx.self_num);
    LOG(msg, sizeof(msg));
}


void validate_args(int argc, char** argv)
{
    /* User must enter exactly one argument */
    char* prog_name = basename(argv[0]);
    if (argc != 2)
        show_usage_n_halt(prog_name);
    /* Check if argument seems to not be a key */
    if (argv[1][0] != '-')
        return;
    /* Compare entered argument with known keys */
    if (strcmp(argv[1], ARG_KEY_HELP) == 0)
    {
        show_usage(prog_name);
        exit(EXIT_SUCCESS);
    }
    /* Unknown key was entered */
    show_usage_n_halt(prog_name);
}


void show_usage_n_halt(char* prog_name)
{
    fprintf(stderr, "ERROR: Invalid program usage! Read the help below.\n\n");
    show_usage(prog_name);
    exit(EXIT_FAILURE);
}


void show_usage(char* prog_name)
{
    printf("NAME\n\n\t");
    printf("%s - a worker for testing IPC", prog_name);

    printf("\n\nSYNOPSIS\n\n\t");
    printf("%s -h|(1|2)", prog_name);

    printf("\n\nDESCRIPTION\n\n\t");
    printf("Starts a worker with specified number for testing IPC.\n\t");
    printf("There could be only one worker with a certain number.\n\t");
    printf("Each worker creates a pipe at \"%s.X\" to reading from, where X "
           "is worker's number.\n\t", PIPE_PREFIX);
    printf("If there is no pipe created by other worker, current worker will "
           "fall asleep waiting for co-worker.\n\t");
    printf("Since there are two awake workers, they will asynchronously send "
           "random messages to each other at random time \n\t");
    printf("interval in rage from %d to %d seconds.\n\t",
            MIN_MSG_INTERVAL, MAX_MSG_INTERVAL);
    printf("Each write and read operations are logged to a remote log over "
           "TCP. It is supposed, that there is a logging \n\t");
    printf("server running at %s:%d.", HOST_ADDR, LISTENING_PORT);

    printf("\n\n\t(1|2)\t");
    printf("worker's number");
    printf("\n\t-h\t");
    printf("show this help");

    printf("\n\nUSAGE EXAMPLE\n\n\t");
    printf("%s 1", prog_name);
    printf("\n\n");
}


/*
 * Set up interruption signals handling to exit program in a proper way and
 * handler of signal from asynchronous timer
 */
void init_signals(void)
{
    if (signal(SIGINT, on_int) == SIG_ERR)
    {
        perror("Failed to set handler for SIGINT");
        exit(EXIT_FAILURE);
    }
    if (signal(SIGTERM, on_int) == SIG_ERR)
    {
        perror("Failed to set handler for SIGTERM");
        exit(EXIT_FAILURE);
    }
    if (signal(SIGALRM, on_alarm) == SIG_ERR)
    {
        perror("Failed to set handler for SIGALRM");
        exit(EXIT_FAILURE);
    }
    if (signal(SIGPIPE, SIG_IGN) == SIG_ERR)
    {
        perror("Failed to ignore SIGPIPE");
        exit(EXIT_FAILURE);
    }
}


/* Interruption handler */
void on_int(int sig_num)
{
    tear_down();
    exit(EXIT_SUCCESS);
}


/*
 * Asynchronous timer's signal handler. Send random messages to peer in random
 * time intervals
 */
void on_alarm(int sig_num)
{
    if (sig_num != -1)
    {
        ctx.msg_id++;

        /* Select random message */
        int idx = rand() % (sizeof(ipsums) / sizeof (*ipsums));
        char buff[MSG_MAX_LENGTH-10];
        int buff_len = sprintf(buff, "[%d.%d] %s",
            ctx.self_num, ctx.msg_id, ipsums[idx]);

        /* Enlog message sending */
        char msg[MSG_MAX_LENGTH+1];
        int log_len = sprintf(msg, "#%d -> #%d: %s",
            ctx.self_num, ctx.peer_num, buff);
        LOG(msg, log_len+1);

        /* Send message to peer */
        size_t i = 0;
        for (i = 0; i < buff_len; i += write(ctx.pipes[1],
                                             buff + i,
                                             buff_len - i));
        /* Finish message with line ending */
        char c = '\n';
        write(ctx.pipes[1], &c, 1);
    }

    /* Set random interval for sending next message */
    int timeout = (rand() % (MAX_MSG_INTERVAL - MIN_MSG_INTERVAL)) +
                  MIN_MSG_INTERVAL;
    alarm(timeout);
}


/* Try to parse number of current worker specified by user */
int get_worker_number(int argc, char** argv)
{
    int result = atoi(argv[1]);
    if (result < 1 || result > 2)
        show_usage_n_halt(basename(argv[0]));
    else
        return result;
}


/* Try to open own input pipe for reading and peer's input pipe for writing */
void get_pipes(char* self_path, int path_len, int self_num, int (*out)[2])
{
    /* Get peer's number */
    int peer = -1;
    ctx.peer_num = 1 + (self_num % 2);

    /* Get paths to pipes */
    char peer_path[path_len];
    char* fmt = "%s.%d";

    memset(self_path, 0, path_len);
    sprintf(self_path, fmt, PIPE_PREFIX, self_num);

    memset(peer_path, 0, path_len);
    sprintf(peer_path, fmt, PIPE_PREFIX, ctx.peer_num);

    /*
     * If own pipe already exists, assume there is a process running with the
     * same worker's number as specified by user
     */
    if(access(self_path, F_OK) != -1 )
    {
        fprintf(
            stderr,
            "File \"%s\" exists! Is a worker #%d already running?\n",
            self_path, self_num);
        exit(EXIT_FAILURE);
    }

    /* Create own pipe */
    mkfifo(self_path, 0666);

    /* If peer exists, open it's pipe for writing and wake it up */
    if(access(peer_path, F_OK) != -1 )
        peer = open(peer_path, O_WRONLY);
    else
        errno = 0;

    /*
     * Open own pipe for reading. This will block current worker until coworker
     * will appear
     */
    (*out)[0] = open(self_path, O_RDONLY);

    /* Open peer's pipe for writing if cowoker spawned after current worker */
    if (peer == -1)
        peer = open(peer_path, O_WRONLY);

    (*out)[1] = peer;
}


/* Try to create socket for communication with remote logger */
int get_logger(void)
{
    socklen_t socksize = sizeof(struct sockaddr_in);
    struct sockaddr_in addr_info;
    int sock = socket(AF_INET, SOCK_STREAM, 0);

    if (sock == -1)
    {
        perror("Socket creation error");
        exit(EXIT_FAILURE);
    }

    memset(&addr_info, 0, socksize);
    addr_info.sin_family = AF_INET;
    addr_info.sin_addr.s_addr = inet_addr(HOST_ADDR);
    addr_info.sin_port = htons(LISTENING_PORT);

    if (connect(sock, (struct sockaddr *)&addr_info, socksize) == -1)
    {
        perror("Socket connection error");
        exit(EXIT_FAILURE);
    } else {
        return sock;
    }
}


/* Enlog message via remote logger */
void enlog(int logger, char* msg, int length)
{
    size_t total = 0;
    size_t done;

    while (total < length)
    {
        done = send(logger, msg + total, length - total, 0);
        if (done == -1)
        {
            perror("Log sending error");
            ctx.logger = -1;
            tear_down();
            exit(EXIT_FAILURE);
        }
        total += done;
    }
}


void tear_down(void)
{
    /* Close pipe descriptors */
    close(ctx.pipes[1]);
    close(ctx.pipes[0]);

    /* Remove own pipe */
    unlink(ctx.self_path);

    /* If connection with logger was not broken */
    if (ctx.logger != -1)
    {
        /* Enlog finishing of the work */
        char msg[20];
        sprintf(msg, "Worker #%d has gone.", ctx.self_num);
        LOG(msg, sizeof(msg));

        close(ctx.logger);
    }
}
